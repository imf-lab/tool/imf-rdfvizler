.PHONY: all test install
.PRECIOUS: example/%.rdf

# DATA
data-input := $(wildcard data/**/*.ttl)
data-output = $(data-input:.ttl=.svg)

# SOFTWARE
JENA-version = apache-jena-4.10.0
JENA = bin/$(JENA-version)

RVIZ = bin/rdfvizler.jar

$(RVIZ):
	mkdir -p bin
	wget -O $@ https://github.com/dyreriket/rdfvizler/releases/download/v0.0.2/rdfvizler-0.0.2.jar

$(JENA):
	mkdir -p bin
	wget https://archive.apache.org/dist/jena/binaries/$(JENA-version).zip -O $@.zip
	unzip -u $@.zip -d bin

install: \
	bin/rdfvizler.jar

%.svg: %.ttl imf-rdfviz.rule install
	java -jar $(RVIZ) --inputFormatRDF=ttl -r imf-rdfviz.rule $< > $@

all: $(data-output)

clean:
	rm -f $(data-output)
	rm -fR bin

test:
	echo $(data-output)
